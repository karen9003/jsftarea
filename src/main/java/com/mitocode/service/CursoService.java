package com.mitocode.service;

import java.util.List;

import com.mitocode.dao.CursoDAO;
import com.mitocode.dao.IDAO;
import com.mitocode.model.Curso;

public class CursoService {
	private IDAO dao;
	
	public CursoService(IDAO idao){
		dao= idao;
	}
	
	public List<Curso> listar(){
		return dao.listar();
	}
}
