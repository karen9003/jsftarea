package com.mitocode.mbean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.faces.view.ViewScoped;
import javax.inject.Named;

import com.mitocode.dao.CursoDAO;
import com.mitocode.model.Curso;
import com.mitocode.service.CursoService;

@Named
@ViewScoped
public class CursoBean implements Serializable{
	private List<Curso> listaCurso;
	private Curso curso;
	private CursoService service;
	
	public CursoBean() {		
		listaCurso = new ArrayList<>();
		service= new CursoService(new CursoDAO());
		Listar();
	}
	
	public void Listar()
	{
		this.listaCurso = service.listar();
	}

	public void enviar(Curso cur)
	{
		this.curso=cur;
	}
	
	public Curso getCurso() {
		return curso;
	}


	public void setCurso(Curso curso) {
		this.curso = curso;
	}
	
	public List<Curso> getListaCurso() {
		return listaCurso;
	}

	public void setListaCurso(List<Curso> listaCurso) {
		this.listaCurso = listaCurso;
	}
}
