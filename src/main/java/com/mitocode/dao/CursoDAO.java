package com.mitocode.dao;

import java.util.ArrayList;
import java.util.List;

import com.mitocode.model.Curso;

public class CursoDAO implements IDAO{

	@Override
	public List<Curso> listar()
	{
		List<Curso> lista= new ArrayList<>();
		for (int i = 0; i < 100; i++) {
			Curso cur= new Curso();
			cur.setIdCurso(i);
			cur.setNombre("Java EE");
			cur.setHoras(32);
			lista.add(cur);
		}
		return lista;
	}	
}
